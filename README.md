## Quality scripts

## Setup

```shell
› bundle install

$ echo 'YOUR_API_TOKEN' > api_token # or pass `--token/-t` to the script
```

### Generate a flaky examples report

```shell
› bundle exec bin/flaky_report --help
Usage: bin/flaky_report [options]

    -f, --report-file REPORT_FILE    A JSON flaky specs report file
    -g [file,type,date,reports],     Group by file (default), type, date, or reports
        --group-by
    -l, --limit LIMIT                Limit the number of lines displayed
    -h, --help                       Print help message
```

### Detect a leaked needle in issues and their comments

```shell
› bundle exec bin/leak_detector_in_issues --help
Usage: bin/leak_detector_in_issues.rb [options]

    -t, --token ACCESS_TOKEN         A valid access token
    -p, --project PROJECT_ID         A valid project ID. Can be an integer or a group/project string
    -n, --needle NEEDLE              A string to search
    -a, --api-endpoint API_ENDPOINT  A valid GitLab API endpoint, defaults to https://gitlab.com/api/v4
    -f, --title-filter TITLE_FILTER  Only search issues for which title match this filter
    -h, --help                       Print help message
```

### List all projects in a group (including from subgroups)

```shell
› bundle exec bin/list_projects_of_group --help
Usage: bin/list_projects_of_group [options]

    -t, --token ACCESS_TOKEN         A valid admin access token
    -g, --group GROUP_PATH           A group path
    -u, --dry-run                    Perform a dry run without actually toggling the feature flag
    -h, --help                       Print help message
```

### Toggle a feature flag on or off for a group's subgroups and their projects

Note: You need an admin personal access token to be able to toggle feature flags.

```shell
› bundle exec bin/toggle_feature_flag_for_group_resources --help
Usage: bin/toggle_feature_flag_for_group_resources.rb [options]

    -t, --token ACCESS_TOKEN         A valid admin access token
    -g, --group GROUP_PATH           A group path
    -f, --feature FEATURE_FLAG       A feature flag
    -v, --value                      Pass "false" or "0" to disable the feature flag. Default to "true".
    -u, --dry-run                    Perform a dry run without actually toggling the feature flag
    -h, --help                       Print help message
```

### Compare two pipelines' jobs (useful when changing the CI config)

Pass a first pipeline URL, and another pipeline URL (or a merge request URL) for
which you'd like to compare the list of jobs.

```shell
› bundle exec bin/compare_pipelines_jobs --help
Usage: bin/compare_pipeline_jobs [options] before_pipeline_url [after_pipeline_url|after_mr_url]

    -t, --token ACCESS_TOKEN         A valid access token
    -h, --help                       Print help message
```

### Gather jobs durations

This is a simple script intended to help us notice outliers in our CI jobs
in order to more evenly distribute them.

```shell
› bundle exec bin/build_durations --help
Usage: bin/build_durations [options] pipeline_url

    -t, --token ACCESS_TOKEN         A valid access token
    -f, --files                      Saves the jobs duration to a file
    -h, --help                       Print help message
```

Provide the gitlab.com URL to a pipeline as the argument to `build_durations`:

```shell
› bundle exec bin/build_durations https://gitlab.com/gitlab-org/gitlab-ce/pipelines/10541108
```

#### Example

```text
Minimum: 18 minutes (rspec-pg 0 25)
Maximum: 63 minutes (rspec-pg 15 25)
Median:  37 minutes
Average: 36 minutes

rspec-pg 15 25 26.97m longer than average -- https://gitlab.com/gitlab-org/gitlab-ce/-/jobs/27541981
  Knapsack: 32 minutes / RSpec: 54 minutes / Job: 63 minutes
   322.45  16% spec/features/dashboard/todos/todos_spec.rb
   275.89  14% spec/features/merge_requests/diff_notes_resolve_spec.rb
rspec-pg 2 25 20.38m longer than average -- https://gitlab.com/gitlab-org/gitlab-ce/-/jobs/27541920
  Knapsack: 40 minutes / RSpec: 46 minutes / Job: 56 minutes
   955.15  39% spec/lib/gitlab/git_access_spec.rb
   551.76  22% spec/services/system_note_service_spec.rb
rspec-pg 12 25 17.00m longer than average -- https://gitlab.com/gitlab-org/gitlab-ce/-/jobs/27541956
  Knapsack: 29 minutes / RSpec: 44 minutes / Job: 53 minutes
   483.66  27% spec/features/boards/boards_spec.rb
   190.84  10% spec/services/ci/retry_build_service_spec.rb
rspec-pg 19 25 12.39m longer than average -- https://gitlab.com/gitlab-org/gitlab-ce/-/jobs/27542007
  Knapsack: 31 minutes / RSpec: 43 minutes / Job: 48 minutes
    373.1  19% spec/services/merge_requests/update_service_spec.rb
   260.43  13% spec/services/git_push_service_spec.rb
rspec-pg 11 25 12.37m longer than average -- https://gitlab.com/gitlab-org/gitlab-ce/-/jobs/27541955
  Knapsack: 25 minutes / RSpec: 39 minutes / Job: 48 minutes
   298.56  19% spec/services/todo_service_spec.rb
   242.22  15% spec/lib/gitlab/cycle_analytics/events_spec.rb
rspec-pg 20 25 10.21m longer than average -- https://gitlab.com/gitlab-org/gitlab-ce/-/jobs/27542020
  Knapsack: 23 minutes / RSpec: 36 minutes / Job: 46 minutes
   281.25  19% spec/features/expand_collapse_diffs_spec.rb
rspec-pg 21 25  7.76m longer than average -- https://gitlab.com/gitlab-org/gitlab-ce/-/jobs/27542039
  Knapsack: 20 minutes / RSpec: 36 minutes / Job: 44 minutes
   156.33  12% spec/requests/api/services_spec.rb
   131.13  10% spec/factories_spec.rb
rspec-pg 13 25  4.77m longer than average -- https://gitlab.com/gitlab-org/gitlab-ce/-/jobs/27541965
  Knapsack: 21 minutes / RSpec: 33 minutes / Job: 41 minutes
   247.03  19% spec/requests/lfs_http_spec.rb
rspec-pg 18 25  3.15m longer than average -- https://gitlab.com/gitlab-org/gitlab-ce/-/jobs/27541991
  Knapsack: 17 minutes / RSpec: 31 minutes / Job: 39 minutes
   106.97  10% spec/requests/api/v3/projects_spec.rb
   116.76  10% spec/features/merge_requests/user_posts_diff_notes_spec.rb
rspec-pg 3 25  3.11m longer than average -- https://gitlab.com/gitlab-org/gitlab-ce/-/jobs/27541923
  Knapsack: 25 minutes / RSpec: 36 minutes / Job: 39 minutes
   600.51  39% spec/features/issues/filtered_search/filter_issues_spec.rb
   187.73  12% spec/requests/api/v3/merge_requests_spec.rb
rspec-pg 22 25  2.37m longer than average -- https://gitlab.com/gitlab-org/gitlab-ce/-/jobs/27542057
  Knapsack: 20 minutes / RSpec: 30 minutes / Job: 38 minutes
   240.88  20% spec/features/commits_spec.rb
rspec-pg 16 25  1.53m longer than average -- https://gitlab.com/gitlab-org/gitlab-ce/-/jobs/27541982
  Knapsack: 21 minutes / RSpec: 35 minutes / Job: 37 minutes
   185.21  14% spec/features/search_spec.rb
rspec-pg 17 25  0.54m longer than average -- https://gitlab.com/gitlab-org/gitlab-ce/-/jobs/27541984
  Knapsack: 20 minutes / RSpec: 30 minutes / Job: 36 minutes
   321.95  26% spec/features/security/project/internal_access_spec.rb
```

At the top you can see the minimum, maximum, median, and average times. This is
how long the _entire job_ took to run (`started_at` to `finished_at`).

Below that we report details on any job that took longer than the average, in
descending order of duration.

Each job reports how much longer than average it took, as well as the URL for
easy review. Below that, any single file that took longer than 10% of the total
Knapsack-reported time is listed, along with the time in seconds.

### Log latest pipelines of a project

```shell
› bundle exec bin/log_pipelines --help
Usage: bin/log_pipelines [options]

    -t, --token ACCESS_TOKEN         A valid access token
    -p, --project PROJECT_PATH       A project path
    -h, --help                       Print help message
```

### Analyses jobs of a project by stage

```shell
› bundle exec bin/pipeline_stages --help
Usage: bin/pipeline_stages [options]

    -p, --project PROJECT_PATH       A project path
    -j, --job JOB_NAME               A job name to get average duration for
    -h, --help                       Print help message
```

### Analyses pipeline timings

```shell
› bundle exec bin/pipeline_timings --help
Usage: bin/pipeline_timings [options]

    -t, --token ACCESS_TOKEN         A valid access token
    -p, --project PROJECT_PATH       A project path
    -h, --help                       Print help message
```

### Analyses runner timings

```shell
› bundle exec bin/runner_timings --help
Usage: bin/runner_timings [options]

    -t, --token ACCESS_TOKEN         A valid access token
    -p, --project PROJECT_PATH       A project path
    -h, --help                       Print help message
```

### Pipeline job report

Script for reporting success rates of `ops.gitlab.net` project's jobs that occur on `master`.

```shell
› bundle exec bin/pipeline_report -h
usage: bin/pipeline_report [options]

Options:
    -h, --help      Shows this help message
    -p, --project   Project for report generation, defaults to gitlab-org/quality/staging
    -d, --num-days  Number of days to look back
```

#### Example

```shell
› bundle exec bin/pipeline_report -d 10 -p gitlab-org/quality/canary
```

```
+--------------+-------------------------------+-------+-----------+--------------+--------------+--------------+
| Stage        | Job                           | Count | Pass Rate | Avg Duration | Max Duration | Min Duration |
+--------------+-------------------------------+-------+-----------+--------------+--------------+--------------+
| cleanup      | qa-schedules-subgroup-cleanup | 117   | 99        | 153.11       | 482.78       | 78.73        |
| notification | notify-slack-success          | 115   | 99        | 17.87        | 268.28       | 11.87        |
| sanity       | qa-schedules-smoke-ff         | 116   | 0         | 147.86       | 792.02       | 103.15       |
| sanity       | qa-schedules-smoke-quarantine | 116   | 86        | 183.25       | 859.49       | 91.56        |
| sanity       | qa-schedules-smoke            | 118   | 97        | 405.11       | 1301.45      | 157.96       |
| sanity       | qa-triggers-smoke-quarantine  | 5     | 100       | 133.38       | 191.37       | 107.63       |
| sanity       | qa-triggers-smoke             | 5     | 100       | 387.0        | 435.56       | 346.5        |
| notification | notify-slack-fail             | 4     | 100       | 20.64        | 34.39        | 13.12        |
+--------------+-------------------------------+-------+-----------+--------------+--------------+--------------+
```

### Failed jobs report

Script for listing failed jobs in `gitlab-org/gitlab`.

```shell
› bundle exec bin/failed_jobs_report --help
Usage: bin/failed_jobs_report [options]

    -t, --token ACCESS_TOKEN         A valid access token
        --hours HOURS                Number of hours to look back - default: 1
    -o, --output OUTPUT_TYPE         Specify output type (console, slack) - default: console
    -j, --job JOB_NAME               The name of the job to get failure data for
    -d, --debug                      Print debugging information
    -h, --help                       Print help message
```

#### Example

```shell
› bundle exec bin/failed_jobs_report --hours 5 --job 'review-deploy'
+--------------+-----------------+--------------------------------+--------------+--------------------------------------------------------------+
|      id      |       name      |           created_at           |    status    |                            web_url                           |
+--------------+-----------------+--------------------------------+--------------+--------------------------------------------------------------+
|    404966883 | review-deploy   | 2020-01-16T06:31:17.985Z       | failed       | https://gitlab.com/gitlab-org/gitlab/-/jobs/404966883        |
+--------------+-----------------+--------------------------------+--------------+--------------------------------------------------------------+
```

#### Pro-tip: Modify the Slack template the easy way

* Generate the Slack report locally. For instance, `bundle exec bin/failed_jobs_report --hours 5 --job 'review-deploy' --output=slack`
* Visit https://app.slack.com/block-kit-builder
* Copy the content of your report (locally under `failed_jobs_report.json`), and remove the first four attributes (i.e. `channel`, `username`, `icon_emoji` and `text`)

#### Generating slack compatible report

To generate a slack compatible report in a json format, use the option `--output=slack`.

### Quarantine Report

#### Develop

```shell
› bundle install
› bundle exec bin/quarantine_report <gitlab-project-directory>
```

After `quarantine_report` has run, it will generate a `quarantine_report.json` file. This file is the raw JSON sent to
Slack.

#### CI Configuration

| Environment Variable | Usage | Default |
| -------------------- | ----- | ------- |
| `QUARANTINE_REPORT_TYPE` | Report type to render. Possible options are `default` and `truncated` | `default` |

### Get the logs for all instances of a specified job

```shell
› bundle exec bin/get_job_logs --help
Usage: bin/get_job_logs [options]

    -t, --token ACCESS_TOKEN         A valid access token
    -p, --project PROJECT_PATH       A project path
    -j, --job JOB_NAME               The name of the job to get logs for
    -s, --scope SCOPE                Comma-separated job status scope. Returns all jobs by default
    -h, --help                       Print help message
```

Logs are saved to `logs/#{project_path}/jobs/#{job_name}/#{started_at}_#{job.id}.log`

### Machine type analysis

Useful to compare different machine types (you'll need to specify the pipelines ID in `bin/machine_types_analysis`).
For reference, that was used for https://gitlab.com/gitlab-org/gitlab/-/issues/322726.

```shell
› bundle exec bin/machine_types_analysis --help
Usage: bin/machine_types_analysis [options] pipeline_url

    -t, --token ACCESS_TOKEN         A valid access token
    -p, --project PROJECT_PATH       A project path
    -s, --standardize                Standardize duration based on n1-standard-2 price ratio
    -h, --help                       Print help message
```

### Log latest merge requests of a project

```shell
› bundle exec bin/log_merge_requests.rb --help
Usage: bin/log_merge_requests.rb [options]

    -t, --token ACCESS_TOKEN         A valid access token
    -p, --project PROJECT_PATH       A project path
    -s [opened|closed|locked|merged],
        --state                      A state to filter MRs on
    -l, --labels LABELS_LIST         A list of labels to filter MRs on
    -h, --help                       Print help message
```

### Analyze paths touched by merge requests

This script will analyse MRs that you saved locally with `bin/log_merge_requests.rb`.

```shell
› bundle exec bin/merge_requests_changes.rb --help
Usage: bin/merge_requests_changes.rb [options]

    -p, --project PROJECT_PATH       A project path
    -s [opened|closed|locked|merged],
        --state                      A state to filter MRs on
    -l, --labels LABELS_LIST         A list of labels to filter MRs on
    -d, --depth FOLDER_DEPTH         The depth to group the change by
    -e, --exclude FOLDERS            A list of folders to ignore
    -L, --limit SHOW_TOP_N_RESULTS   The number of top results to show
    -h, --help                       Print help message
```

### Analyze retried tests

This script will analyse retried tests in the `master` branch. You'll need to log pipelines first with
`bundle exec bin/log_pipelines`.

This looks at the `rspec/flaky/retried_tests_report.txt` artifact data from the `rspec:flaky-tests-report` job.

```shell
› bundle exec bin/retried_tests_analysis.rb --help
Usage: bin/retried_tests_analysis.rb [options]

    -t, --token ACESS_TOKEN          A valid access token
    -p, --project PROJECT_PATH       A project path
    -h, --help                       Print help message
```

### Merge request templates

This script will analyse the merge commit templates for projects that are listed at
https://gitlab.com/gitlab-data/analytics/-/blob/master/transform/snowflake-dbt/data/projects_part_of_product.csv.

You can also set the default merge commit template to all these projects by passing the `--action update`/`-a update` option.

```shell
› bundle exec bin/merge_commit_templates.rb --help
Usage: bin/merge_commit_templates.rb [options]

    -t, --token ACCESS_TOKEN         A valid access token
    -a, --action [list|update]       A valid action: `list` templates, `update` templates - default: list
    -d, --debug                      Print debugging information
    -h, --help                       Print help message
```

### Community MR Participants

Script has been moved to https://gitlab.com/gitlab-org/community-relations/contributor-success/toolbox

### "Autoclose referenced issues" setting

This script will analyse the `autoclose_referenced_issues` setting for projects that are listed at
https://gitlab.com/gitlab-data/analytics/-/blob/master/transform/snowflake-dbt/data/projects_part_of_product.csv.

```shell
› bundle exec bin/autoclose_referenced_issues.rb --help
Usage: bin/autoclose_referenced_issues.rb [options]

    -t, --token ACCESS_TOKEN         A valid access token
    -d, --debug                      Print debugging information
    -h, --help                       Print help message
```
