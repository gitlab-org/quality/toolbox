#!/usr/bin/env ruby
# frozen_string_literal: true

require 'bundler/setup'
require 'csv'
require 'optparse'
require 'gitlab'

require File.expand_path('../lib/token_finder.rb', __dir__)

Options = Struct.new(
  :token,
  :action,
  :debug
)

class AnalyzeMergeCommitTemplatesOptionParser
  def self.parse(argv)
    args = Options.new
    args.action = :list

    OptionParser.new do |opts|
      opts.banner = "Usage: #{__FILE__} [options]\n\n"

      opts.on('-t', '--token ACCESS_TOKEN', String, 'A valid access token') do |value|
        args.token = value
      end

      opts.on('-a', '--action [list|update]', String, %i[list update], 'A valid action: `list` templates, `update` templates - default: list') do |value|
        args.action = value.to_sym
      end

      opts.on('-d', '--debug', 'Print debugging information') do |value|
        args.debug = value
      end

      opts.on('-h', '--help', 'Print help message') do
        $stdout.puts opts
        exit
      end
    end.parse!(argv)

    args
  end
end

class AnalyzeMergeCommitTemplates
  CSV_URL = 'https://gitlab.com/gitlab-data/analytics/-/raw/master/transform/snowflake-dbt/data/projects_part_of_product.csv'
  GITLAB_API = 'https://gitlab.com/api/v4'
  # rubocop:disable Style/FormatStringToken
  DEFAULT_MERGE_COMMIT_TEMPLATE = <<~TEMPLATE.chomp
    Merge branch '%{source_branch}' into '%{target_branch}'

    %{title}

    %{issues}

    See merge request %{url}

    Merged-by: %{merged_by}
    %{approved_by}
    %{reviewed_by}
    %{co_authored_by}
  TEMPLATE
  # rubocop:enable Style/FormatStringToken

  def initialize(options)
    @token = TokenFinder.find_token!(options.token)
    @action = options.action

    Gitlab.configure do |config|
      config.endpoint = GITLAB_API
      config.private_token = token
    end
  end

  def execute
    case action
    when :list
      data = analyze(show_progress: true)
      show_summary(data)
    when :update
      update
    else
      raise ArgumentError, "Unsupported action #{action}!"
    end
  end

  def analyze(show_progress: false)
    return JSON.parse(File.read(log_path)) if File.exist?(log_path)

    puts "Analyzing merge commit templates for #{projects.size} projects..."
    merge_commit_template_per_project = projects.each_with_object({}) do |project, memo|
      print '.' if show_progress

      memo[project['project_path']] = Gitlab.project(project['project_path']).merge_commit_template.to_s
    rescue Gitlab::Error::NotFound
      puts "\nProject #{project['project_path']} (##{project['project_id']}) couldn't be found!"
    end

    File.write(log_path, merge_commit_template_per_project.to_json)

    merge_commit_template_per_project
  end

  def update
    templates_per_project = analyze

    templates_per_project.each do |project_path, template|
      # Only update Quality projects for now
      next unless project_path.start_with?('gitlab-org/quality') || project_path == 'gitlab-org/ruby/gems/gitlab-styles'
      next if template == DEFAULT_MERGE_COMMIT_TEMPLATE

      puts "Merge commit template for #{project_path} is not conform:"
      p template

      Gitlab.edit_project(project_path, { merge_commit_template: DEFAULT_MERGE_COMMIT_TEMPLATE })
      templates_per_project[project_path] = DEFAULT_MERGE_COMMIT_TEMPLATE
    rescue Gitlab::Error::NotFound
      puts "\nProject #{project_path} couldn't be found!"
    rescue Gitlab::Error::Forbidden
      puts "\nCurrent token doesn't have permission to update template in #{project_path}!"
    end

    File.write(log_path, templates_per_project.to_json)

    templates_per_project
  end

  private

  attr_reader :token, :action

  def log_path
    'logs/merge_request_templates_for_projects_part_of_product.json'
  end

  def projects
    @projects ||= retrieve_csv(CSV_URL)
      .map(&:to_hash)
      .each { |hash| hash['project_id'] = hash['project_id'].to_i }
  end

  def retrieve_csv(csv_url)
    response = HTTParty.get(csv_url)
    CSV.parse(response.parsed_response, headers: :first_row)
  end

  def show_summary(merge_commit_template_per_project)
    complying_templates, rest = merge_commit_template_per_project.partition { |_k, template| template == DEFAULT_MERGE_COMMIT_TEMPLATE }
    complying_projects = complying_templates.map(&:first)
    no_templates, non_complying_templates = rest.partition { |_k, v| v.to_s.empty? }.map(&:to_h)

    if complying_projects.any?
      puts "The following #{complying_projects.size} projects have a complying merge commit template:\n"
      complying_projects.each { |project_path| puts "- #{project_path}" }
    end

    if non_complying_templates.any?
      puts "\nThe following #{non_complying_templates.size} projects have a non-complying merge commit template:\n"
      non_complying_templates.each { |project_path, template| puts "- #{project_path} #{template.inspect}" }
    end

    return if no_templates.empty?

    puts "\nThe following #{no_templates.size} projects do not have a merge commit template:\n"
    no_templates.each { |project_path, _v| puts "- #{project_path}" }
  end
end

if $PROGRAM_NAME == __FILE__
  options = AnalyzeMergeCommitTemplatesOptionParser.parse(ARGV)
  start = Time.now
  engine = AnalyzeMergeCommitTemplates.new(options)
  engine.execute

  puts "\nDone in #{Time.now - start} seconds."
end
