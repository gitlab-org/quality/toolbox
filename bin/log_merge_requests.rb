#!/usr/bin/env ruby
# frozen_string_literal: true

# Loop through all merge requests in the last month, and write
# job data to a log file unique to each pipeline (e.g.,
# `logs/gitlab-org/gitlab/merge_requests/12345678.json') for later processing.

require 'bundler/setup'
require 'optparse'
require 'gitlab'
require 'fileutils'
require 'active_support/core_ext/integer'

require File.expand_path('../lib/token_finder.rb', __dir__)
require File.expand_path('../lib/time_helpers.rb', __dir__)

Options = Struct.new(
  :token,
  :project_path,
  :state,
  :labels
)

class LogMergeRequestsOptionParser
  def self.parse(argv)
    args = Options.new

    OptionParser.new do |opts|
      opts.banner = "Usage: #{__FILE__} [options]\n\n"

      opts.on('-t', '--token ACCESS_TOKEN', String, 'A valid access token') do |value|
        args.token = value
      end

      opts.on('-p', '--project PROJECT_PATH', String, 'A project path') do |value|
        args.project_path = value
      end

      opts.on('-s', '--state [opened|closed|locked|merged]', String, 'A state to filter MRs on') do |value|
        args.state = value
      end

      opts.on('-l', '--labels LABELS_LIST', Array, 'A list of labels to filter MRs on') do |value|
        args.labels = value
      end

      opts.on('-h', '--help', 'Print help message') do
        $stdout.puts opts
        exit
      end
    end.parse!(argv)

    args
  end
end

class LogMergeRequests
  GITLAB_API = 'https://gitlab.com/api/v4'
  CUTOFF = 1.month.ago

  def initialize(options)
    @token = TokenFinder.find_token!(options.token)
    @project_path = options.project_path
    @state = options.state
    @labels = options.labels

    assert_project_path!

    Gitlab.configure do |config|
      config.endpoint = GITLAB_API
      config.private_token = token
    end
  end

  def process
    FileUtils.mkdir_p(File.join(File.expand_path('../logs', __dir__), project_path, 'merge_requests'))

    opts = { per_page: 100 }
    opts[:state] = state if state
    opts[:labels] = labels if labels

    merge_requests = Gitlab.merge_requests(project_path, **opts)

    merge_requests.auto_paginate do |mr|
      log_path = "logs/#{project_path}/merge_requests/#{mr.id}.json"
      next if File.exist?(log_path)

      puts "--> #{mr.iid} (state: #{mr.state}, labels: #{mr.labels})"

      mr_changes = Gitlab.merge_request_changes(project_path, mr.iid)
      File.write(log_path, mr_changes.to_h.to_json)
    end
  end

  private

  attr_reader :token, :project_path, :state, :labels

  def assert_project_path!
    return if project_path

    $stderr.warn 'Please provide a valid project_path with the `-p/--project` option!'
    exit 1
  end
end

if $PROGRAM_NAME == __FILE__
  options = LogMergeRequestsOptionParser.parse(ARGV)
  start = Time.now
  engine = LogMergeRequests.new(options)
  engine.process

  puts "\nDone in #{Time.now - start} seconds."
end
