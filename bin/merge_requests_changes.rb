#!/usr/bin/env ruby
# frozen_string_literal: true

require 'bundler/setup'
require 'optparse'
require 'json'
require 'active_support/core_ext/hash/indifferent_access'

Options = Struct.new(
  :token,
  :project_path,
  :state,
  :labels,
  :depth,
  :exclude,
  :limit
)

class MergeRequestChangesOptionParser
  def self.parse(argv)
    args = Options.new

    OptionParser.new do |opts|
      opts.banner = "Usage: #{__FILE__} [options]\n\n"

      opts.on('-p', '--project PROJECT_PATH', String, 'A project path') do |value|
        args.project_path = value
      end

      opts.on('-s', '--state [opened|closed|locked|merged]', String, 'A state to filter MRs on') do |value|
        args.state = value
      end

      opts.on('-l', '--labels LABELS_LIST', Array, 'A list of labels to filter MRs on') do |value|
        args.labels = value
      end

      opts.on('-d', '--depth FOLDER_DEPTH', Integer, 'The depth to group the change by') do |value|
        args.depth = value
      end

      opts.on('-e', '--exclude FOLDERS', Array, 'A list of folders to ignore') do |value|
        args.exclude = value
      end

      opts.on('-L', '--limit SHOW_TOP_N_RESULTS', Integer, 'The number of top results to show') do |value|
        args.limit = value
      end

      opts.on('-h', '--help', 'Print help message') do
        $stdout.puts opts
        exit
      end
    end.parse!(argv)

    args
  end
end

class MergeRequestChanges
  MERGE_REQUESTS_ATTRS = %i[iid state labels changes].freeze
  DEFAULT_TOP_RESULTS_COUNT = 50
  MergeRequest = Struct.new(*MERGE_REQUESTS_ATTRS, keyword_init: true)

  def initialize(options)
    @project_path = options.project_path
    @state = options.state
    @labels = options.labels
    @depth = options.depth || 2
    @exclude = options.exclude || []
    @limit = options.limit || DEFAULT_TOP_RESULTS_COUNT

    assert_project_path!
  end

  def process
    process_by_folder
  end

  private

  attr_reader :project_path, :state, :labels, :depth, :exclude, :limit

  def assert_project_path!
    return if project_path

    $stderr.warn 'Please provide a valid project_path with the `-p/--project` option!'
    exit 1
  end

  def merge_request_files
    @merge_request_files ||= Dir["logs/#{project_path}/merge_requests/*.json"].sort
  end

  def process_by_folder
    mr_counts = 0
    stats_per_folder = Hash.new { |h, k| h[k] = 0 }

    merge_requests do |mr|
      next if state.nil? || mr.state != state
      next if labels.nil? || (mr.labels & labels) != labels

      mr_counts += 1

      mr.changes.each do |change|
        folder_at_depth = change['new_path'].delete_prefix('ee/').split('/')[0...depth].join('/')
        next if exclude.any? { |folder| folder_at_depth.start_with?(folder) }

        stats_per_folder[folder_at_depth] += 1
      end
    end
    puts "Analysing changes for #{mr_counts} MRs..."
    puts
    puts "Showing the top first #{limit} changes excluding #{exclude.join(', ')}:"

    stats_per_folder = stats_per_folder.sort_by { |_k, v| -v }.first(limit)

    stats_per_folder.each_with_index do |(folder, count), index|
      puts "#{index + 1}. #{folder} => #{count} (#{((count / mr_counts.to_f) * 100).ceil(2)}% of the #{mr_counts} are touching this path)"
    end
    puts "\nNote that the `ee/` prefix is removed."
  end

  def merge_requests
    merge_request_files.each do |path|
      mr = JSON.parse(File.read(path)).with_indifferent_access

      yield MergeRequest.new(**mr.slice(*MERGE_REQUESTS_ATTRS))
    end
  end
end

if $PROGRAM_NAME == __FILE__
  options = MergeRequestChangesOptionParser.parse(ARGV)
  start = Time.now
  engine = MergeRequestChanges.new(options)
  engine.process

  puts "\nDone in #{Time.now - start} seconds."
end
