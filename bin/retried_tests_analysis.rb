#!/usr/bin/env ruby
# frozen_string_literal: true

require 'bundler/setup'
require 'optparse'
require 'csv'
require 'gitlab'
require 'fileutils'

require_relative '../lib/token_finder'

Options = Struct.new(
  :token,
  :project_path
)

JOB_ATTRS = %i[id name].freeze
Job = Struct.new(*JOB_ATTRS, keyword_init: true) do
  def retried_jobs
    @retried_jobs ||= Set.new
  end

  def retried_tests
    @retried_tests ||= Set.new
  end

  def inspect
    %(#<struct Job id="#{id}", name="#{name}" retried_jobs=#{retried_jobs} retried_tests=#{retried_tests}>)
  end
end

module RetriedTestsAnalysisOptionParser
  def self.parse(argv)
    args = Options.new

    OptionParser.new do |opts|
      opts.banner = "Usage: #{__FILE__} [options]\n\n"

      opts.on('-t', '--token ACESS_TOKEN', String, 'A valid access token') do |value|
        args.token = value
      end

      opts.on('-p', '--project PROJECT_PATH', String, 'A project path') do |value|
        args.project_path = value
      end

      opts.on('-h', '--help', 'Print help message') do
        $stdout.puts opts
        exit
      end
    end.parse!(argv)

    args
  end
end

class RetriedTestsAnalysis
  GITLAB_API = 'https://gitlab.com/api/v4'
  REPORT_JOB_NAME = 'rspec:flaky-tests-report'
  ARTIFACT_PATH = 'rspec/flaky/retried_tests_report.txt'

  def initialize(options)
    @token = TokenFinder.find_token!(options.token)
    @project_path = options.project_path

    assert_project_path!

    Gitlab.configure do |config|
      config.endpoint = GITLAB_API
      config.private_token = token
    end
  end

  def process
    process_for_job
  end

  private

  attr_reader :token, :project_path

  def assert_project_path!
    return if project_path

    warn 'Please provide a valid project_path with the `-p/--project` option!'
    exit 1
  end

  def pipelines
    @pipelines ||= Dir["logs/#{project_path}/pipelines/*.csv"].sort
  end

  def process_for_job
    test_report_jobs_with_retries = []
    pipelines_with_tests_count = 0

    pipeline_and_jobs do |_pipeline, jobs|
      test_report_job_with_retries = jobs.find { |job| job.name == REPORT_JOB_NAME }
      next if test_report_job_with_retries.nil?

      retried_tests_report = retrieve_test_report(test_report_job_with_retries.id)

      next if retried_tests_report.nil?

      pipelines_with_tests_count += 1

      next if retried_tests_report.empty?

      retried_jobs = retried_tests_report.lines.grep(%r{^https://gitlab\.com})
        .map(&:chomp)
      retried_tests = retried_tests_report.lines
        .grep(%r{\./.*\] \|}) # matches ./spec/services/ci/create_pipeline_service/logger_spec.rb[1:1:3:1] | failed | 0.43177 seconds |\n
        .flat_map { |line| line.split('./') }
        .map { |item| item.gsub(/\].*/, ']').chomp }
        .reject(&:empty?)

      test_report_job_with_retries.retried_jobs.merge(retried_jobs)
      test_report_job_with_retries.retried_tests.merge(retried_tests)

      test_report_jobs_with_retries << test_report_job_with_retries
    end

    display_summary(test_report_jobs_with_retries, pipelines_with_tests_count)
  end

  def retrieve_test_report(job_id)
    artifact_cache_path = File.join(File.expand_path('../logs', __dir__), project_path, 'jobs', job_id, "artifacts/#{ARTIFACT_PATH}")

    if File.exist?(artifact_cache_path)
      File.read(artifact_cache_path)
    else
      artifact = Gitlab.get(
        "/projects/#{Gitlab.client.url_encode(project_path)}/jobs/#{job_id}/artifacts/#{ARTIFACT_PATH}",
        format: nil,
        headers: { Accept: 'application/octet-stream' },
        parser: proc { |body, _|
                  case body.encoding
                  when Encoding::ASCII_8BIT, Encoding::UTF_8 # binary response
                    ::Gitlab::FileResponse.new StringIO.new(body, 'rb+')
                  else # error with json response
                    ::Gitlab::Request.parse(body)
                  end
                }
      )

      return unless artifact

      FileUtils.mkdir_p(File.dirname(artifact_cache_path))
      artifact.read.tap do |report_content|
        File.binwrite(artifact_cache_path, report_content)
      end
    end
  end

  def display_summary(test_report_jobs_with_retries, pipelines_with_tests_count)
    puts

    if test_report_jobs_with_retries.empty?
      puts "No job named #{job_name} were found!"
    else
      total_retried_jobs = test_report_jobs_with_retries.sum { |job| job.retried_jobs.size }
      average_retried_jobs_per_pipeline = total_retried_jobs / pipelines_with_tests_count.to_f
      total_retried_tests = test_report_jobs_with_retries.sum { |job| job.retried_tests.size }
      average_retried_tests_per_pipeline = total_retried_tests / pipelines_with_tests_count.to_f

      retries_per_test = test_report_jobs_with_retries.each_with_object(Hash.new { |h, k| h[k] = 0 }) do |job, memo|
        job.retried_tests.each { |test| memo[test] += 1 }
        memo
      end.sort_by(&:last).reverse.to_h

      puts "Number of pipeline analyzed: #{pipelines_with_tests_count}"
      puts "Number of pipeline with retries: #{test_report_jobs_with_retries.size}"
      puts "Ratio of pipeline that passed after retries: #{((test_report_jobs_with_retries.size / pipelines_with_tests_count.to_f) * 100).ceil}%"
      puts "Average retried jobs per pipeline: #{average_retried_jobs_per_pipeline}"
      puts "Average retried tests per pipeline: #{average_retried_tests_per_pipeline}"
      puts 'Retries per test:'
      retries_per_test.each do |test, count|
        puts "- #{test} was retried #{count} times (average of #{count / pipelines_with_tests_count.to_f} per pipeline)"
      end
    end
  end

  def pipeline_and_jobs
    pipelines.each do |path|
      pipeline = File.basename(path, '.csv')

      jobs = []

      CSV.foreach(path, headers: true) do |line|
        jobs << Job.new(id: line['id'], name: line['job'])
      end

      puts "--> #{pipeline}"

      yield pipeline, jobs
    end
  end
end

if $PROGRAM_NAME == __FILE__
  options = RetriedTestsAnalysisOptionParser.parse(ARGV)
  start = Time.now
  engine = RetriedTestsAnalysis.new(options)
  engine.process

  puts "\nDone in #{Time.now - start} seconds."
end
