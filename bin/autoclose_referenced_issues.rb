#!/usr/bin/env ruby
# frozen_string_literal: true

require 'bundler/setup'
require 'csv'
require 'optparse'
require 'gitlab'

require File.expand_path('../lib/token_finder.rb', __dir__)

Options = Struct.new(
  :token,
  :debug
)

class AnalyzeAutocloseReferencedIssuesOptionParser
  def self.parse(argv)
    args = Options.new

    OptionParser.new do |opts|
      opts.banner = "Usage: #{__FILE__} [options]\n\n"

      opts.on('-t', '--token ACCESS_TOKEN', String, 'A valid access token') do |value|
        args.token = value
      end

      opts.on('-d', '--debug', 'Print debugging information') do |value|
        args.debug = value
      end

      opts.on('-h', '--help', 'Print help message') do
        $stdout.puts opts
        exit
      end
    end.parse!(argv)

    args
  end
end

class AnalyzeAutocloseReferencedIssues
  CSV_URL = 'https://gitlab.com/gitlab-data/analytics/-/raw/master/transform/snowflake-dbt/data/projects_part_of_product.csv'
  GITLAB_API = 'https://gitlab.com/api/v4'

  def initialize(options)
    @token = TokenFinder.find_token!(options.token)

    Gitlab.configure do |config|
      config.endpoint = GITLAB_API
      config.private_token = token
    end
  end

  def execute
    data = analyze(show_progress: true)
    show_summary(data)
  end

  def analyze(show_progress: false)
    return JSON.parse(File.read(log_path)) if File.exist?(log_path)

    puts "Analyzing `autoclose_referenced_issues` for #{projects.size} projects..."
    autoclose_referenced_issues_per_project = projects.each_with_object({}) do |project, memo|
      print '.' if show_progress

      memo[project['project_path']] = Gitlab.project(project['project_path']).autoclose_referenced_issues
    rescue Gitlab::Error::NotFound
      puts "\nProject #{project['project_path']} (##{project['project_id']}) couldn't be found!"
    end

    File.write(log_path, autoclose_referenced_issues_per_project.to_json)

    autoclose_referenced_issues_per_project
  end

  private

  attr_reader :token

  def log_path
    'logs/autoclose_referenced_issues_for_projects_part_of_product.json'
  end

  def projects
    @projects ||= retrieve_csv(CSV_URL)
      .map(&:to_hash)
      .each { |hash| hash['project_id'] = hash['project_id'].to_i }
  end

  def retrieve_csv(csv_url)
    response = HTTParty.get(csv_url)
    CSV.parse(response.parsed_response, headers: :first_row)
  end

  def show_summary(autoclose_referenced_issues_per_project)
    autoclose_referenced_issues_true, autoclose_referenced_issues_false = autoclose_referenced_issues_per_project.partition { |_k, value| value }
    autoclose_referenced_issues_true_projects = autoclose_referenced_issues_true.map(&:first)
    autoclose_referenced_issues_false_projects = autoclose_referenced_issues_false.map(&:first)

    if autoclose_referenced_issues_true_projects.any?
      puts "The following #{autoclose_referenced_issues_true_projects.size} projects have `autoclose_referenced_issues` set to `true`:\n"
      autoclose_referenced_issues_true_projects.each { |project_path| puts "- #{project_path}" }
    end

    return unless autoclose_referenced_issues_false_projects.any?

    puts "\nThe following #{autoclose_referenced_issues_false_projects.size} projects have `autoclose_referenced_issues` set to `false`:\n"
    autoclose_referenced_issues_false_projects.each { |project_path| puts "- #{project_path}" }
  end
end

if $PROGRAM_NAME == __FILE__
  options = AnalyzeAutocloseReferencedIssuesOptionParser.parse(ARGV)
  start = Time.now
  engine = AnalyzeAutocloseReferencedIssues.new(options)
  engine.execute

  puts "\nDone in #{Time.now - start} seconds."
end
