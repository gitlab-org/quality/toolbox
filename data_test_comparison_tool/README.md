# Data Test Comparison Tool
## Purpose
With us shifting to having multiple instances of GitLab running in parallel and moving data between instances (Cells), having the ability to verify that the data in those Cells is correct in an efficient manner is more critical.

Doing this below the UI layer provides several benefits:
1. Speed: directly querying the data in the database is faster than walking through the UI and scraping the data.
1. Reliabily: We can hide where the data is coming from in the UI. For example, Geo has implemented a redirect that if the data is not in the secondary, the request automatically gets redirected to the primary.
1. Efficiency: If we're only querying through the UI, we're at the end of the chain and dependant on the UI being automated and the underlying logic being correct. For example, if we wanted to verify the number of comments on an MR, through the UI you'ld have to count all the comments on the that MR page, check if there's another page, count that one,... With querying the database, it's a single query to the database and compare against the number displayed.

Work being done on this are being tracked in [this Epic](https://gitlab.com/groups/gitlab-org/quality/quality-engineering/-/epics/25). The current work is focused on [Cells](https://about.gitlab.com/direction/core_platform/tenant-scale/cell/), so will be targetting running against a GDK based architecture initially. 

This folder contains a version of the Tool that runs independantly of other testing tools to enable easier exploration and iteration.

## Script description

The scripts are setup to be run from commandline and have notes about how to use in the script.

| Script name | Description | Sample Syntax |
| ----------- | ----------- | ------------- |
| [compare_databases.rb](./compare_databases.rb) | Compares two GDK databases and checks that the projects and namespaces are unique between two cells | `./compare_databases.rb ~/src/gitlab-development-kit ~/src/gdk2` |
| [includes_project.rb](./includes_project.rb) | Checks if a project is in the GDK database | `./includes_project.rb add-member-project-0880b742c7aff31f ~/src/gitlab-development-kit/` |



