#!/usr/bin/env ruby
# frozen_string_literal: true

# gdk_url = '~/src/gitlab-development-kit/'
# gdk2_url = '~/src/gdk2/'

def compare(query, gdk_url, gdk2_url)
  # query = "\"#{in_query}\""
  command = "gdk psql -t -c  '#{query}'"
  gdk_result = `cd #{gdk_url}; #{command}`
  gdk2_result = `cd #{gdk2_url}; #{command}`

  gdk_project = gdk_result.split("\n")
  gdk2_project = gdk2_result.split("\n")

  gdk_project & gdk2_project
end

# --- execution below ---

if ARGV.size == 2
  gdk_url = ARGV[0]
  gdk2_url = ARGV[1]
else
  puts 'How to run:'
  puts './compare_databases.rb <path to Cell1 GDK> <path to Cell2 GDK>'
  exit(1)
end

# Compare namespaces
namespaces_in_common = compare('select name from namespaces;', gdk_url, gdk2_url)

if namespaces_in_common.empty?
  puts 'No namespaces in common between both cells'
else
  puts "Namespaces found in both Cells: #{namespaces_in_common.join(', ')}"
  exit 1
end

# Compare projects
projects_in_common = compare('select name from projects;', gdk_url, gdk2_url)

if projects_in_common.empty?
  puts 'No projects in common between both cells'
else
  puts "Projects found in both Cells: #{projects_in_common.join(', ')}"
  exit 1
end
