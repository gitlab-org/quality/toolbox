#!/usr/bin/env ruby
# frozen_string_literal: true

# gdk_url = '~/src/gitlab-development-kit/'
# project_name = 'add-member-project-0880b742c7aff31f'

def include_project?(project_name, gdk_url)
  query = 'select name from projects;' # search for all because query gets truncated by psql if too long
  command = "gdk psql -t -c  '#{query}'"
  gdk_result = `cd #{gdk_url}; #{command}`

  gdk_project = gdk_result.split("\n ")

  gdk_project.include?(project_name)
end

# --- execution below ---

if ARGV.size == 2
  project_name = ARGV[0]
  gdk_url = ARGV[1]
else
  puts 'How to run:'
  puts './includes_project.rb <project_name> <path to Cell GDK>'
  exit(1)
end

if include_project?(project_name, gdk_url)
  puts "Project #{project_name} found in Cell"
else
  puts "Project #{project_name} not found in Cell"
  exit 1
end
