# frozen_string_literal: true

require 'json'
require 'httparty'

class StageSetMapper
  LINK = 'https://gitlab-org.gitlab.io/gitlab-roulette/roulette.json'
  attr_accessor :stages_set_hash

  def initialize
    @roulette_json = {}
    @stages_set_hash = {}
  end

  def parse_json
    response = HTTParty.get(LINK)
    @roulette_json = JSON.parse(response.body)
  end

  def create_roulette_set_hash
    # Parse roulette json to construct a hash with stage:[SET array] mapping
    parse_json
    @roulette_json.each do |item|
      next unless item['role'].include? 'software-engineer-in-test'

      stage = if item['role'].include? ','
                item['role'].split(':')[0].split(',')[-1].strip.downcase
              else
                item['role'].split(':')[0].split('a>')[-1].strip.downcase
              end
      (@stages_set_hash[stage] ||= []) << item['username']
    end
    @stages_set_hash
  end
end
